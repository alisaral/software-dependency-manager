import datetime
import time
import json

class Vertex:

	outgoing_edges = {}

class Edge:

	def __init__(self, src, target):
		self.src = src
		self.target = target

class App():

	def __init__(self, string_representation):
		self.and_dependencies = []
		self.or_dependencies = []
		split = string_representation.split('#')
		self.name = split[0]
		if len(split) > 1:
			self.version = split[1]
		else:
			self.version = 0
		self.support_until = datetime.datetime(1980, 1, 1, 1, 1, 1).date()

	def set_support_deadline(self, **kwargs):
		self.support_until = datetime.date(kwargs['year'], kwargs['month'], kwargs['day'])

	def is_supported_at(self, date):
		return date > self.support_until

	def __repr__(self):
		return self.__str__()

	def __str__(self):
		return self.name + '#' + self.version

	def requires_one_of(self, apps):
		self.or_dependencies.append(apps)
		return self

	def requires(self, app):
		self.and_dependencies.append(app)
		return self

class ValidationManager:

	def __init__(self):
		self.validations = {}

	def add(self, label, validation):
		if not label in self.validations:
			self.validations[label] = []
		self.validations[label].append(validation)

	def __repr__(self):
		return self.__str__()

	def __str__(self):
		return json.dumps(self.validations)

class AppRepository:

	def __init__(self):
		self.apps = {}

	def add(self, app):
		self.apps[str(app)] = app
		return self

	def add_all(self, apps):
		for app in apps:
			self.add(app)
		return self

	def get(self, string_representation):
		return self.apps[string_representation]

	def from_string(self, app_repr_list):
		result = []
		for string_representation in app_repr_list:
			app = self.get(string_representation)
			result.append(app)
		return result

class EnvironmentValidator:

	def __init__(self, *checkers):
		self.checkers = checkers

	def validate(self, environment, app_repo):
		visited = []
		validations = ValidationManager()
		for app in environment:
			self.visit(app, environment, app_repo, validations, visited)
		return validations

	def visit(self, app, environment, app_repo, validations, visited):
		if app in visited:
			return
		visited.append(app)
		for checker in self.checkers:
			checker.check(app, environment, app_repo, validations)
		for dependency in app.and_dependencies:
			self.visit(dependency, environment, app_repo, validations, visited)
		for dependency in app.or_dependencies:
			self.visit(dependency, environment, app_repo, validations, visited)

class MissingDependencyChecker:

	def check(self, app, environment, app_repo, validations):
		self.check_and_dependencies(app, environment, validations)
		self.check_or_dependencies(app, environment, validations)

	def check_and_dependencies(self, app, environment, validations):
		for dependency in app.and_dependencies:
			if not dependency in environment:
				validations.add('missing_dependency', str(app) + ' requires ' + str(dependency))

	def check_or_dependencies(self, app, environment, validations):
		for dependencies in app.or_dependencies:
			self.check_or_dependency(app, dependencies, environment, validations)
			intersection = list(set(dependencies) & set(environment))
			if len(intersection) == 0:
				validations.add('missing_dependency', str(app) + ' requires one of ' + dependencies)

class SupportChecker:

	def __init__(self, date):
		self.date = date

	def check(self, app, environment, app_repo, validations):
		if app.is_supported_at(self.date):
			validations.add('support', str(app) + ' support ends with ' + str(app.support_until) + '. App is not supported on ' + str(self.date))

class CSVImporter:

	def read_apps(self, filename, ignore_first_line):
		apps = []
		f = open(filename, 'r')
		for line in f:
			if ignore_first_line:
				ignore_first_line = False
			else:
				app = self.parse_line(line.strip())
				apps.append(app)
		f.close()
		return apps

	def read_environment(self, filename, repo):
		env = []
		f = open(filename, 'r')
		for line in f:
			env.append(line.strip())
		f.close()
		return repo.from_string(env)

	def parse_line(self, line):
		cells = line.split('\t')
		app = App(cells[0])
		app.version = cells[1]
		support_date = self.parse_date(cells[2])
		app.set_support_deadline(year=support_date.year, month=support_date.month, day=support_date.day)
		return app

	def parse_date(self, text):
		return datetime.datetime.strptime(text, "%d.%m.%Y")






