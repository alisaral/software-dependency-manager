from models import *
import sys
from pygments import highlight
from pygments.lexers import JsonLexer
from pygments.formatters import HtmlFormatter

def usage():
	print ("python run.py <apps.csv> <environment.csv> <date as dd.MM.YYYY>")

def create_repo_from_file(filename):
	importer = CSVImporter()
	apps = importer.read_apps(filename, True)
	repo = AppRepository()
	repo.add_all(apps)
	return (repo)

def create_environment_from_file(filename, repo):
	importer = CSVImporter()
	return importer.read_environment(filename, repo)

if __name__ == '__main__':
	if len(sys.argv) != 4:
		usage()
	else:
		repo = create_repo_from_file(sys.argv[1])
		environment = create_environment_from_file(sys.argv[2], repo)
		validator = EnvironmentValidator(MissingDependencyChecker(), SupportChecker(datetime.datetime.strptime(sys.argv[3], "%d.%m.%Y").date()))
		validations = validator.validate(environment, repo)
		print (highlight(str(validations), JsonLexer(), HtmlFormatter(style='colorful', noclasses=True)))
