import unittest
from models import *

class CheckEnvironmentConsistency(unittest.TestCase):

	def test_check(self):
		repo = AppRepository()
		jre = App('JRE#1.7')
		jre.set_support_deadline(year=2015, month=12, day=12)

		eap = App("JBOSS_EAP#5.2")
		eap.set_support_deadline(year=2015, month=12, day=12)
		eap.requires(jre)

		rap = App("RAP#14.5")
		rap.set_support_deadline(year=2015, month=12, day=12)
		rap.requires(jre).requires(eap)

		repo.add(jre).add(rap).add(eap)

		environment = repo.from_string(['JRE#1.7', 'RAP#14.5', "JBOSS_EAP#5.2"])

		validator = EnvironmentValidator(MissingDependencyChecker(), SupportChecker(datetime.datetime.utcnow().date()))
		print (validator.validate(environment, repo))

	def test_read_from_file(self):
		importer = CSVImporter()
		apps = importer.read_apps("test-data.csv", True)
		repo = AppRepository()
		repo.add_all(apps)

		print (apps)



if __name__ == '__main__':
	unittest.main()